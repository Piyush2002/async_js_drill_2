
const boardData=require("./boards.json");
const callback1=require("./callback1.cjs");
const callback2=require("./callback2.cjs");
const callback3=require("./callback3.cjs");


function information(callback) {
    setTimeout(() => {

        let thanosId=null;
        boardData.find((ele)=>{
            if(ele.name === "Thanos"){
                thanosId=ele.id;
            }
        })
        
        callback1(thanosId,(err,data)=>{
            if(err){
                console.log(err);
            }
            else{
                console.log(data);
                callback2(thanosId,(err,data)=>{
                    if(err){
                        console.log(err);
                    }
                    else{
                        console.log(data);

                        let mindElement=null;
                        let spaceID=null;
                        data.find((ele) => {
                            if(ele.name === "Mind"){
                                mindElement=ele.id;
                            }
                            if(ele.name === "Space"){
                                spaceID=ele.id;
                            }
                        });
        
                        if (mindElement || spaceID) {
                            callback3(thanosId,(err,data)=>{
                                if(err){
                                    console.log(err);
                                }
                                else{
                                    Object.entries(data).find((ele)=>{
                                        if(ele[0]===mindElement){
                                            console.log(ele[1]);
                                        }
                                        if(ele[0]===spaceID){
                                            console.log(ele[1]);
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            console.log("not found in data");
                        }
                    }
                });
            }
        });
        
        
        
    }, 2 * 1000);
}

module.exports = information;
