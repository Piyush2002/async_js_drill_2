const listData = require("./lists.json");
const cardData = require("./cards.json");

function information(id, callback) {
    setTimeout(() => {
        const res = Object.entries(listData).reduce((acc, curr) => {
            if (curr[0] === id) {
                acc = curr[1];
            }
            return acc;
        }, null);

        const resultObject = res.reduce((acc, ele) => {
            const listID = ele.id;

            const cards = Object.entries(cardData).reduce((cardsAcc, curr) => {
                if (curr[0] === listID) {
                    cardsAcc=(curr[1]);
                }
                return cardsAcc;
            }, []);

            if (cards.length > 0) {
                acc[listID] = cards;
            }

            return acc;
        }, {});

        if (Object.keys(resultObject).length > 0) {
            callback(null, resultObject);
        } else {
            callback(new Error("No matching entries found in card data"));
        }
    }, 2 * 1000);
}

module.exports = information;
