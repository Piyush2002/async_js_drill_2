
const data=require("./boards.json");

function information(idGiven,board){
    setTimeout(()=>{
        const res=data.filter((ele)=>{
            if(idGiven===ele.id){
                return ele;
            }
        })
        
        if(res.length >0){
            board(null,res);
        }
        else {
            board(new Error("Entry not found"));
        }

    },2*1000)
}


module.exports=information;