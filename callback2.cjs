const listData = require("./lists.json");

function information(id, callback) {
    setTimeout(() => {
        const res = Object.entries(listData).reduce((acc, curr) => {
            
            if (curr[0] === id) {
                acc=curr[1];
            }

            return acc;

        }, []);

        if (res.length > 0) {
            callback(null, res);
        } 
        else {
            callback(new Error("Entry not found"));
        }

    }, 2 * 1000);
}

module.exports = information;
